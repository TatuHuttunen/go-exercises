package hello

//TestVersion indicates the solution's version in "exercism.io"
const TestVersion = 2

//World greets you happily!
func World(name string) string {
	if len(name) > 0 {
		return "Hello, " + name + "!"
	}
	return "Hello, World!"
}
