package hello

import "testing"

const targetTestVersion = 2

func TestHelloWorld(t *testing.T) {
	tests := []struct {
		name, expected string
	}{
		{"", "Hello, World!"},
		{"Gopher", "Hello, Gopher!"},
	}
	for _, test := range tests {
		observed := World(test.name)
		if observed != test.expected {
			t.Fatalf("HelloWorld(%s) = %v, want %v", test.name, observed, test.expected)
		}
	}

	if TestVersion != targetTestVersion {
		t.Fatalf("Found testVersion = %v, want %v", TestVersion, targetTestVersion)
	}
}
