package etl

import "strings"

// Original format for old scores
type Original map[int][]string

// Transformed format for scores in the new system
type Transformed map[string]int

// Transform translates scores found from the original format to the new format
func Transform(input Original) Transformed {
	out := Transformed{}
	for val, letters := range input {
		setValueToLetters(letters, val, out)
	}
	return out
}

func setValueToLetters(ls []string, val int, tr Transformed) {
	for _, l := range ls {
		tr[strings.ToLower(l)] = val
	}
}
