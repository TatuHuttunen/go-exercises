package clock

import (
	"fmt"
	"math"
)

// The value of testVersion here must match `targetTestVersion` in the file
// clock_test.go.
const testVersion = 4
const secondsInADay = 86400
const secondsInAnHour = 3600

// Clock type handles times without timezone or date
type Clock struct {
	seconds int
}

// New takes in hours and minutes and creates a new clock instance
func New(hour, minute int) Clock {
	c := Clock{0}
	return c.Add((hour * 60) + minute)
}

func (c Clock) String() string {
	h := int(math.Floor(float64(c.seconds / secondsInAnHour)))
	m := int(math.Floor(float64(c.seconds / 60 % 60)))
	return fmt.Sprintf("%02d:%02d", h, m)
}

// Add adds/subtracts minutes from an existing clock
func (c Clock) Add(minutes int) Clock {
	s := minutes * 60
	sec := c.seconds + s
	subtractWholeDays(&sec)
	convertNegative(&sec)
	return Clock{seconds: sec}
}

func subtractWholeDays(i *int) {
	days := int(math.Abs(math.Floor(float64(*i / secondsInADay))))
	if days >= 1 {
		if *i > 0 {
			*i -= days * secondsInADay
		} else {
			*i += days * secondsInADay
		}
	}
}

func convertNegative(i *int) {
	if *i < 0 {
		*i = secondsInADay + *i
	}
}
