package secret

import "strconv"

// Handshake converts a given number to sequence of events
func Handshake(num int) []string {
	if num < 0 {
		return nil
	}

	steps := []string{}
	for _, act := range getAvailableActions() {
		if act.isContainedInInt(num) {
			steps = act.commit(steps)
		}
	}
	return steps
}

func getAvailableActions() []handShakeAction {
	actions := []handShakeAction{}
	return append(actions,
		addMessage{action{"1"}, "wink"},
		addMessage{action{"10"}, "double blink"},
		addMessage{action{"100"}, "close your eyes"},
		addMessage{action{"1000"}, "jump"},
		reverse{action{"10000"}},
	)
}

type handShakeAction interface {
	isContainedInInt(int) bool
	commit([]string) []string
}

type action struct {
	code string
}

func (a action) isContainedInInt(i int) bool {
	co, err := strconv.ParseInt(a.code, 2, 0)
	if err != nil {
		return false
	}
	return (int(co) & i) == int(co)
}

type addMessage struct {
	action
	message string
}

func (a addMessage) commit(s []string) []string {
	return append(s, a.message)
}

type reverse struct {
	action
}

func (a reverse) commit(s []string) []string {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return s
}
