package gigasecond

import "time"

const testVersion = 4

//AddGigasecond will add one Gigasecond (10*9) to given date
func AddGigasecond(bDay time.Time) time.Time {
	gigaAnniversary := bDay.Add(time.Second * 1000000000)
	return gigaAnniversary
}
