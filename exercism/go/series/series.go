package slice

// All returns all n-length sub-strings from string s
func All(n int, s string) []string {
	var subs []string
	for i := range s {
		if sub, ok := First(n, s[i:]); ok {
			subs = append(subs, sub)
		}
	}
	return subs
}

// UnsafeFirst returns first n length sub-string from string s.
// Does not check sub-strings length.
func UnsafeFirst(n int, s string) string {
	return s[:n]
}

// First returns first n length sub-string from string s.
// Check the given length and returns false as the second value if invalid
func First(n int, s string) (string, bool) {
	if n > len(s) {
		return "", false
	}
	return s[:n], true
}
