package scrabble

import "strings"

const testVersion = 4

var points = map[int]string{
	1:  "AEIOULNRST",
	2:  "DG",
	3:  "BCMP",
	4:  "FHVMY",
	5:  "k",
	8:  "JX",
	10: "QZ",
}

var cache = map[rune]int{}

// Score calculates point value for given string
func Score(in string) int {
	in = strings.ToLower(strings.TrimSpace(in))
	tot := 0
	for _, l := range in {
		tot += getValue(l)
	}
	return tot
}

func getValue(l rune) int {
	if len(cache) == 0 {
		setCache()
	}
	return cache[l]
}

func setCache() {
	for val, ls := range points {
		ls = strings.ToLower(ls)
		for _, l := range ls {
			cache[l] = val
		}
	}
}
