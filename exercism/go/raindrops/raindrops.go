package raindrops

import (
	"bytes"
	"strconv"
)

const testVersion = 2

type conversion struct {
	num int
	str string
}

var conversions = []conversion{
	{3, "Pling"},
	{5, "Plang"},
	{7, "Plong"},
}

// Convert returns a given number converted to string based on factors of 3, 5 and 7
func Convert(num int) string {
	var b bytes.Buffer

	for _, cv := range conversions {
		if num%cv.num == 0 {
			b.WriteString(cv.str)
		}
	}

	st := b.String()
	if len(st) == 0 {
		return strconv.Itoa(num)
	}
	return st
}
