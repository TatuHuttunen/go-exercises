package triangle

import (
	"math"
	"sort"
)

const testVersion = 2

const (
	// NaT is a kind identifier used for invalid triangles
	NaT = "notATriangle"
	// Equ is a kind identifier for equilateral triangles
	Equ = "equilateral"
	// Iso is a kind identifier for isosceles triangles
	Iso = "isosceles"
	// Sca is a kind identifier for scalene triangles
	Sca = "scalene"
)

// Kind specifies relation between lengths of triangles sides
type Kind string

// KindFromSides resolves triangles kind from given side lengths
func KindFromSides(a, b, c float64) Kind {
	tr := triangle{a, b, c}
	switch {
	case !tr.isValid():
		return NaT
	case tr.isEquilateral():
		return Equ
	case tr.isIsosceles():
		return Iso
	default:
		return Sca
	}
}

type triangle struct {
	a, b, c float64
}

func (t triangle) isValid() bool {
	ls := []float64{t.a, t.b, t.c}
	if !validSideLengths(ls) || !triangleInequality(ls) {
		return false
	}
	return true
}

func (t triangle) isEquilateral() bool {
	return (t.a == t.b) && (t.a == t.c)
}

func (t triangle) isIsosceles() bool {
	if t.a == t.b || t.a == t.c || t.b == t.c {
		return true
	}
	return false
}

func validSideLengths(ls []float64) bool {
	for _, i := range ls {
		if i <= 0 || math.IsNaN(i) || math.IsInf(i, 1) {
			return false
		}
	}
	return true
}

func triangleInequality(ls []float64) bool {
	sort.Float64s(ls)
	return ((ls[0] + ls[1]) >= ls[2])
}
