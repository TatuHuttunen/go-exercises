package queenattack

import (
	"errors"
	"strconv"
	"strings"
)

type piece struct {
	column string
	row    int
}

func newPiece(pos string) (piece, error) {
	xs := strings.SplitAfter(pos, "")
	if len(xs) != 2 {
		return piece{}, errors.New("invalid piece coordinates")
	}
	col, cErr := formatColumn(xs[0])
	row, rErr := formatRow(xs[1])
	return piece{col, row}, checkErrors(cErr, rErr)
}

func (p piece) getPosAsInts() (int, int) {
	return strings.Index(columns, p.column) + 1, p.row
}

func (p piece) isDiagonalTo(t piece) bool {
	return isInDiagonalDirection(p, t)
}

func formatColumn(in string) (string, error) {
	for _, c := range columns {
		if in == string(c) {
			return in, nil
		}
	}
	return "", errors.New("invalid piece column value")
}

func formatRow(s string) (int, error) {
	i, err := strconv.Atoi(s)
	if err != nil {
		return 0, err
	}
	if 0 < i && i <= numOfRows {
		return i, nil
	}
	return 0, errors.New("invalid piece row value")
}
