package queenattack

import "errors"

type queen struct {
	piece
}

func newQueen(pos string) (queen, error) {
	p, err := newPiece(pos)
	return queen{p}, checkErrors(err)
}

func (q queen) canAttackTarget(target piece) (bool, error) {
	//same column and same row
	if q.column == target.column && q.row == target.row {
		return false, errors.New("invalid target position")
	}
	//same column or same row
	if q.column == target.column || q.row == target.row {
		return true, nil
	}
	if q.isDiagonalTo(target) {
		return true, nil
	}
	return false, nil
}
