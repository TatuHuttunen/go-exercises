package queenattack

func isInDiagonalDirection(c, t piece) bool {
	for _, d := range diagonalDirections {
		i, j := c.getPosAsInts()
		tI, tJ := t.getPosAsInts()
		for d.checkCol(i) && d.checkRow(j) {
			if i == tI && j == tJ {
				return true
			}
			d.next(&i, &j)
		}
	}
	return false
}

var diagonalDirections = []directionChecker{
	rightUp{},
	rightDown{},
	leftUp{},
	leftDown{},
}

type directionChecker interface {
	checkCol(int) bool
	checkRow(int) bool
	next(*int, *int)
}

type rightUp struct{}

func (r rightUp) checkCol(i int) bool {
	return i <= len(columns)
}
func (r rightUp) checkRow(j int) bool {
	return j <= numOfRows
}
func (r rightUp) next(i *int, j *int) {
	*i++
	*j++
}

type rightDown struct{}

func (r rightDown) checkCol(i int) bool {
	return i <= len(columns)
}
func (r rightDown) checkRow(j int) bool {
	return j > 0
}
func (r rightDown) next(i *int, j *int) {
	*i++
	*j--
}

type leftUp struct{}

func (l leftUp) checkCol(i int) bool {
	return i > 0
}
func (l leftUp) checkRow(j int) bool {
	return j <= numOfRows
}
func (l leftUp) next(i *int, j *int) {
	*i--
	*j++
}

type leftDown struct{}

func (l leftDown) checkCol(i int) bool {
	return i > 0
}
func (l leftDown) checkRow(j int) bool {
	return j > 0
}
func (l leftDown) next(i *int, j *int) {
	*i--
	*j--
}
