package queenattack

const columns = "abcdefgh"
const numOfRows = 8

// CanQueenAttack checks if a queen in first given position can attack a
// queen in second position.
func CanQueenAttack(posA, posT string) (bool, error) {
	attQ, errA := newQueen(posA)
	tarQ, errC := newQueen(posT)
	if err := checkErrors(errA, errC); err != nil {
		return false, err
	}

	can, errC := attQ.canAttackTarget(tarQ.piece)
	return can, checkErrors(errC)
}

func checkErrors(errs ...error) error {
	for _, e := range errs {
		if e != nil {
			return e
		}
	}
	return nil
}
