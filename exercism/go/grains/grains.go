package grains

import (
	"errors"
	"math"
)

// Square returns the count of grains in a single square.
// Number of grains is always equal to 2^(square - 1)
func Square(sq int) (uint64, error) {
	if 0 < sq && sq <= 64 {
		return uint64(math.Pow(2, float64(sq-1))), nil
	}
	return 0, errors.New("invalid square")
}

// Total return total number of grains in the board.
// Number of grains in total is always the number of grains in the next
// square minus one. e.g: total in first three squares is 7 (2^4) -1,
// total in first 4 is 15 (2^5) - 1 etc. So total in first 64 is 2^65 -1
func Total() uint64 {
	tot, err := Square(64)
	if err != nil {
		return 0
	}
	return tot*2 - 1
}
