package diffsquares

// SquareOfSums returns square of the first n natural numbers summed
func SquareOfSums(n int) int {
	// sum of arithmetic series from 1 to n
	sums := (n * (n + 1)) / 2
	return sums * sums
}

// SumOfSquares returns the sum of first n natural numbers squared
func SumOfSquares(n int) int {
	// https://proofwiki.org/wiki/Sum_of_Sequence_of_Squares
	return (n * (n + 1) * (2*n + 1)) / 6
}

// Difference returns the difference between squared sum and sum of squares
// for first n natural numbers
func Difference(n int) int {
	return SquareOfSums(n) - SumOfSquares(n)
}
