package hamming

import "errors"

const testVersion = 4

// Distance counts Hamming distance between to DNA strands
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return -1, errors.New("hamming distance is only defined for sequences of equal length")
	}

	d := 0
	for i := range a {
		if a[i] != b[i] {
			d++
		}
	}
	return d, nil
}
