package foodchain

import "strings"

const (
	testVersion = 2
	numOfVerses = 8
)

type verseData struct {
	animal  string
	desc    string
	toCatch string
}

var verseStructure = map[int]verseData{
	1: {"fly", "", "fly"},
	2: {"spider", "It wriggled and jiggled and tickled inside her.", "spider that wriggled and jiggled and tickled inside her"},
	3: {"bird", "How absurd to swallow a bird!", ""},
	4: {"cat", "Imagine that, to swallow a cat!", ""},
	5: {"dog", "What a hog, to swallow a dog!", ""},
	6: {"goat", "Just opened her throat and swallowed a goat!", ""},
	7: {"cow", "I don't know how she swallowed a cow!", ""},
}

// Verse returns songs verse by verse's number
func Verse(num int) string {
	if num == numOfVerses {
		return "I know an old lady who swallowed a horse.\nShe's dead, of course!"
	}
	v := swallowAnimal(num)
	v += addPreviousAnimals(num)
	v += "I don't know why she swallowed the fly. Perhaps she'll die."
	return v
}

// Verses returns Song's verses from given verse to limit
func Verses(from, to int) string {
	vs := []string{}
	for i := from; i <= to; i++ {
		vs = append(vs, Verse(i))
	}
	return strings.Join(vs, "\n\n")
}

// Song returns all eight verses of the song
func Song() string {
	return Verses(1, 8)
}

func swallowAnimal(num int) string {
	st := "I know an old lady who swallowed a " + verseStructure[num].animal + ".\n"
	if len(verseStructure[num].desc) > 0 {
		st += verseStructure[num].desc + "\n"
	}
	return st
}

func addPreviousAnimals(num int) string {
	st := ""
	for i := num; i > 1; i-- {
		catch := verseStructure[i-1].toCatch
		if len(catch) == 0 {
			catch = verseStructure[i-1].animal
		}
		st += "She swallowed the " + verseStructure[i].animal + " to catch the " + catch + ".\n"
	}
	return st
}
