package pascal

// Triangle returns first 'num' rows of Pascal's Triangle
func Triangle(num int) [][]int {
	rows := [][]int{}
	for i := 0; i < num; i++ {
		if i == 0 {
			rows = append(rows, []int{1})
			continue
		}
		rows = append(rows, getTriangleRow(rows[i-1]))
	}
	return rows
}

func getTriangleRow(prev []int) []int {
	row := []int{1}
	for j := len(prev) - 1; j > 0; j-- {
		row = append(row, prev[j]+prev[j-1])
	}
	return append(row, 1)
}
