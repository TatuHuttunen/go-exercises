package leap

// testVersion should match the targetTestVersion in the test file.
const testVersion = 2

// IsLeapYear determines whether the given year is a leap year or not
func IsLeapYear(y int) bool {
	if y%4 == 0 {
		if y%100 == 0 && (y%400 != 0) {
			return false
		}
		return true
	}
	return false
}
