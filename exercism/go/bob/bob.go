package bob

import (
	"bytes"
	"unicode"
)

const testVersion = 2

// Hey returns bob's answer to given statement
func Hey(in string) string {
	if isShouting([]rune(in)) {
		return "Whoa, chill out!"
	}
	if isQuestion([]byte(in)) {
		return "Sure."
	}
	if isSilence([]byte(in)) {
		return "Fine. Be that way!"
	}

	return "Whatever."
}

func isShouting(str []rune) bool {
	letters := getLetters(str)
	if len(letters) > 0 && isAllUpperCase(letters) {
		return true
	}
	return false
}

func getLetters(str []rune) []rune {
	var letters []rune

	for _, r := range str {
		if unicode.IsLetter(r) {
			letters = append(letters, r)
		}
	}
	return letters
}

func isAllUpperCase(str []rune) bool {
	lowerLetters := 0
	for _, r := range str {
		if unicode.IsLetter(r) && !unicode.IsUpper(r) {
			lowerLetters++
		}
	}
	return (lowerLetters == 0)
}

func isQuestion(str []byte) bool {
	str = bytes.TrimSpace(str)
	if len(str) > 0 {
		return string(str[len(str)-1]) == "?"
	}
	return false
}

func isSilence(str []byte) bool {
	str = bytes.TrimSpace(str)
	return string(str) == ""
}
