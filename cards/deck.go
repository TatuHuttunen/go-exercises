package main

import "fmt"

type deck []card

func newDeck() deck {
	d := deck{}

	for _, s := range suits {
		for _, f := range faces {
			d = append(d, newCard(s, f))
		}
	}
	return d
}

func (d deck) Print() {
	for _, card := range d {
		fmt.Println(card)
	}
}
