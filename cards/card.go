package main

type suit string

type face string

// Faces
var (
	suits = []suit{"Club", "Diamond", "Heart", "Spade"}
	faces = []face{"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"}
)

type card struct {
	suite suit
	face  face
}

func newCard(s suit, f face) card {
	return card{suite: s, face: f}
}
