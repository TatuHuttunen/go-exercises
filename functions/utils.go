package functions

//SplitInt returns given integer split in half and boolean indicating wether the int was even
func SplitInt(num int) (half int, isEven bool) {
	half = num / 2
	isEven = (num%2 == 0)
	return
}

//MaximumInt returns the greatest number of the given numbers
func MaximumInt(numbers ...int) int {
	max := 0
	for _, num := range numbers {
		if num > max {
			max = num
		}
	}
	return max
}

//MakeOddGenerator returns a closure function which will return odd numbers
func MakeOddGenerator() func() uint {
	i := 0
	return func() uint {
		for {
			i++
			if i%2 != 0 {
				return uint(i)
			}
		}
	}
}

//Fib is a recursive function which returns a corresponding number in Fibbonacci sequence
func Fib(count uint) uint {
	if count == 0 || count == 1 {
		return count
	}
	return Fib(count-1) + Fib(count-2)
}

//Swap takes two integers as arguments and returns those in a swapped order
func Swap(x *int, y *int) (int, int) {
	return *y, *x
}

//CountClumps calculates number of clumps in given slice.
//Clump is a case where the same integer is repeated twice or more
func CountClumps(numbers []int) int {
	count := 0
	length := len(numbers)

	for i, num := range numbers {
		switch i {
		case 0:
			continue
		case length - 1:
			if num == numbers[i-1] {
				count++
			}
		default:
			if num == numbers[i-1] && num != numbers[i+1] {
				count++
			}
		}
	}

	return count
}
