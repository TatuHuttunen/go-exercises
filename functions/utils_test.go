package functions

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

var splitTests = []struct {
	number int
	half   int
}{
	{2, 1},
	{5, 2},
	{-15, -7},
	{10, 5},
	{25, 12},
	{0, 0},
}

func TestSplitInt(t *testing.T) {

	for _, st := range splitTests {
		if st.number%2 == 0 {
			Convey("Given a number which can be split evenly by two", t, func() {
				num, even := SplitInt(st.number)

				Convey("It should return half of the number and true", func() {
					So(num, ShouldEqual, st.half)
					So(even, ShouldEqual, true)
				})
			})
		} else {
			Convey("Given an number which can not be split evenly by two", t, func() {
				num, even := SplitInt(st.number)
				Convey("It should return half of the number and false", func() {
					So(num, ShouldEqual, st.half)
					So(even, ShouldEqual, false)
				})
			})
		}
	}
}

var maximumTests = []struct {
	numbers  []int
	greatest int
}{
	{[]int{6, 7, 8, 3, 2}, 8},
	{[]int{0, -7, 28}, 28},
	{[]int{0, 1, -1}, 1},
}

func TestMaximumInt(t *testing.T) {
	for _, mt := range maximumTests {
		Convey("Given a variadic list on numbers", t, func() {
			Convey("It returns the greatest one", func() {
				So(MaximumInt(mt.numbers...), ShouldEqual, mt.greatest)
			})

		})
	}
}

func TestMakeOddGenerator(t *testing.T) {
	odds := []uint{1, 3, 5, 7, 9, 11, 13, 15, 17, 19}
	nextOdd := MakeOddGenerator()
	for _, odd := range odds {
		if got := nextOdd(); got != odd {
			t.Errorf("got %d, expected %d", got, odd)
		}
	}
}

var fibTests = []struct {
	count  uint
	number uint
}{
	{0, 0},
	{1, 1},
	{5, 5},
	{10, 55},
	{30, 832040},
}

func TestFib(t *testing.T) {
	for _, ft := range fibTests {
		Convey("Given a number", t, func() {
			Convey("It returns the corresponding number in Fibonacci sequense", func() {
				So(Fib(ft.count), ShouldEqual, ft.number)
			})
		})
	}
}

var swapTests = []struct {
	x        int
	y        int
	expected [2]int
}{
	{2, 3, [2]int{3, 2}},
	{2, 2, [2]int{2, 2}},
	{4, -5, [2]int{-5, 4}},
	{1000, 10000, [2]int{10000, 1000}},
	{0, 0, [2]int{0, 0}},
}

func TestSwap(t *testing.T) {
	for _, st := range swapTests {
		Convey("When it is given two numbers", t, func() {
			st.x, st.y = Swap(&st.x, &st.y)
			Convey("it returns them in a swapped order", func() {
				So(st.x, ShouldEqual, st.expected[0])
				So(st.y, ShouldEqual, st.expected[1])
			})
		})

	}
}

var clumpCounts = []struct {
	numbers []int
	count   int
}{
	{[]int{}, 0},
	{[]int{1}, 0},
	{[]int{1, 1}, 1},
	{[]int{1, 1, 1, 1, 1, 1}, 1},
	{[]int{1, 2, 3, 4, 5, 6}, 0},
	{[]int{1, 1, 2, 1, 1, 1}, 2},
	{[]int{1, 1, 2, 2, 1, 1}, 3},
	{[]int{1, 2, 2, 2, 2, 2}, 1},
	{[]int{2, 2, 2, 2, 2, 1}, 1},
}

func TestCountClumps(t *testing.T) {
	for _, tc := range clumpCounts {
		if got := CountClumps(tc.numbers); got != tc.count {
			t.Errorf("got %d, expected %d with values %v", got, tc.count, tc.numbers)
		}
	}

}
