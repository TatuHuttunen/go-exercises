package slices

import "testing"

var minTests = []struct {
	numbers []int
	minimum int
}{
	{[]int{1, 2, 4, 5}, 1},
	{[]int{5, 4, 3, 2, 1}, 1},
	{[]int{-5, -4}, -5},
	{[]int{5, 4, 3, 2, 1, -100, 5, 7}, -100},
	{[]int{}, 0},
}

func TestMinimum(t *testing.T) {
	for _, mt := range minTests {
		if got := MinimumInt(mt.numbers...); got != mt.minimum {
			t.Errorf("got: %d, expected: %d for numbers %v", got, mt.minimum, mt.numbers)
		}
	}
}

var sumTests = []struct {
	numbers []int
	sum     int
}{
	{[]int{1, 2, 4, 5}, 12},
	{[]int{5, 4, 3, 2, 1}, 15},
	{[]int{-5, -4}, -9},
	{[]int{5, 4, 3, 2, 1, -100, 5, 7}, -73},
	{[]int{-4, -4, -4, -4, -4}, -20},
	{[]int{}, 0},
}

func TestSum(t *testing.T) {
	for _, st := range sumTests {
		if got := SumInt(st.numbers); got != st.sum {
			t.Errorf("got: %d, expected: %d for numbers %v", got, st.sum, st.numbers)
		}
	}
}
