package slices

//MinimumInt returns the minimum integer of the given arguments
func MinimumInt(numbers ...int) (min int) {
	if len(numbers) == 0 {
		return 0
	}
	min = numbers[0]
	for _, num := range numbers {
		if num < min {
			min = num
		}
	}
	return
}

//SumInt counts total of given integers
func SumInt(numbers []int) int {
	total := 0
	for _, num := range numbers {
		total = total + num
	}
	return total
}
