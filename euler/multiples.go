package euler

import "errors"

const iterationLimit = 100000

//SmallestEvenlyDivisible returns the smallest integer which is divisible by all numbers from 2 to limit
//Returns error if not found
func SmallestEvenlyDivisible(limit int) (int, error) {
	if limit < 1 {
		return 0, nil
	}
	min := 1
	for {
		if canBeDividedEvenlyByAllInRange(min, limit) {
			return min, nil
		}
		min++
		if min > iterationLimit {
			return 0, errors.New("Not Found")
		}

	}
}

func canBeDividedEvenlyByAllInRange(num, rangeMax int) bool {
	for i := 2; i <= rangeMax; i++ {
		if num%i != 0 {
			return false
		}
	}
	return true
}
