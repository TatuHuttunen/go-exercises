package euler

import (
	"testing"

	"errors"

	"github.com/stretchr/testify/assert"
)

var smallestDivisibleCases = []struct {
	limit         int
	smallest      int
	expectedError error
}{
	{0, 0, nil},
	{1, 1, nil},
	{3, 6, nil},
	{10, 2520, nil},
	{15, 0, errors.New("Not Found")},
}

func TestSmallestEvenlyDivisible(t *testing.T) {
	asserts := assert.New(t)
	for _, tc := range smallestDivisibleCases {
		smallest, err := SmallestEvenlyDivisible(tc.limit)
		asserts.Equal(tc.smallest, smallest)
		asserts.Equal(tc.expectedError, err)
	}
}
