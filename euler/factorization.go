package euler

import (
	"math/big"
	"sort"
)

//PrimeFactorize divides given number into prime factories
func PrimeFactorize(number uint) []int {
	var factors []int
	var splitToPrimes func(uint)
	splitToPrimes = func(num uint) {
		x, y := factor(num)
		for _, i := range [2]uint{x, y} {
			if isPrime(i) {
				factors = append(factors, int(i))
			} else {
				splitToPrimes(i)
			}
		}
	}

	splitToPrimes(number)
	sort.Ints(factors)
	return factors
}

func factor(num uint) (uint, uint) {
	var i uint = 2
	for {
		if num%i == 0 {
			return num / i, i
		}
		i++
	}
}

func isPrime(num uint) bool {
	return big.NewInt(int64(num)).ProbablyPrime(20)
}
