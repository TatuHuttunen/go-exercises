package euler

import "exercises/functions"

type intSerializer interface {
	nextInt() int
}

type multiplesOf3Or5 struct {
	current int
}

func (m *multiplesOf3Or5) nextInt() int {
	for {
		m.current++
		if m.current%3 == 0 || m.current%5 == 0 {
			return m.current
		}
	}
}

type evenFibNumbers struct {
	current int
}

func (e *evenFibNumbers) nextInt() int {
	for {
		e.current++
		fib := functions.Fib(uint(e.current))
		if fib%2 == 0 {
			return int(fib)
		}
	}
}

//SumOf3Or5multiples returns the sum of numbers which are divisible by either 3 or 5
func SumOf3Or5multiples(limit uint) uint {
	numbers := getIntegersInSeries(int(limit), &multiplesOf3Or5{current: 0})
	return uint(sumOfNumbers(numbers...))
}

//SumOfEvenFib returns sum of even Fibonacci numbers
func SumOfEvenFib(limit uint) uint {
	numbers := getIntegersInSeries(int(limit), &evenFibNumbers{current: 0})
	return uint(sumOfNumbers(numbers...))
}

func getIntegersInSeries(limit int, serializer intSerializer) []int {
	var numbers []int

	for {
		i := serializer.nextInt()
		if i >= limit {
			break
		}
		numbers = append(numbers, i)
	}
	return numbers
}

func sumOfNumbers(numbers ...int) int {
	total := 0

	for _, num := range numbers {
		total += num
	}
	return total
}
