package euler

import (
	"reflect"
	"testing"
)

var factorizeCases = []struct {
	number    uint
	factories []int
}{
	{12, []int{2, 2, 3}},
	{147, []int{3, 7, 7}},
	{13195, []int{5, 7, 13, 29}},
}

func TestPrimeFactorize(t *testing.T) {
	for _, tc := range factorizeCases {
		if got := PrimeFactorize(tc.number); !reflect.DeepEqual(got, tc.factories) {
			t.Errorf("got %v, expected %v with number %d", got, tc.factories, tc.number)
		}
	}
}
