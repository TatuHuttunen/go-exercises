package euler

import "testing"

var sumOfMultiplesCases = []struct {
	limit uint
	sum   uint
}{
	{0, 0},
	{10, 23},
	{1000, 233168},
}

func TestSumOf3Or5multiples(t *testing.T) {
	for _, tc := range sumOfMultiplesCases {
		got := SumOf3Or5multiples(tc.limit)
		if got != tc.sum {
			t.Errorf("got %d, expected %d for values %v", got, tc.sum, tc)
		}
	}
}

var sumOfEvenFibCases = []struct {
	limit uint
	sum   uint
}{
	{9, 10},
}

func TestSumOfEvenFib(t *testing.T) {
	for _, tc := range sumOfEvenFibCases {
		got := SumOfEvenFib(tc.limit)
		if got != tc.sum {
			t.Errorf("got %d, expected %d for values %v", got, tc.sum, tc)
		}
	}
}
