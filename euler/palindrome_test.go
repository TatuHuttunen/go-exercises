package euler

import "testing"

var palindromeProducts = []struct {
	maxFactor int
	product   int
}{
	{9, 9},
	{99, 9009},
	{999, 906609},
}

func TestLargestPalindromeProduct(t *testing.T) {
	for _, tc := range palindromeProducts {
		if got := LargestPalindromeProduct(tc.maxFactor); got != tc.product {
			t.Errorf("got %v, expected %v with value %v", got, tc.product, tc.maxFactor)
		}
	}
}
